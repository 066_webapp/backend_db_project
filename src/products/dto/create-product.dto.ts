import { IsNotEmpty, Length, IsInt, IsPositive } from 'class-validator';
export class CreateProductDto {
  @Length(4, 25)
  @IsNotEmpty()
  name: string;

  @IsPositive()
  @IsInt()
  @IsNotEmpty()
  price: number;
}
