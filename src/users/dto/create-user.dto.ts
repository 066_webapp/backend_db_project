import { IsNotEmpty, Length, MinLength, Matches } from 'class-validator';
export class CreateUserDto {
  @Length(4, 16)
  @IsNotEmpty()
  login: string;

  @MinLength(4)
  @IsNotEmpty()
  name: string;

  @Matches(/^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,}$/)
  @MinLength(8)
  @IsNotEmpty()
  password: string;
}
